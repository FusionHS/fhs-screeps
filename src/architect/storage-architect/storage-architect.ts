import {FlagType} from "../../flag/flag-type";
import {RoomUtils} from "../../room/room-utils";
import {Architect} from "../architect";

export class StorageArchitect implements Architect {
  public name: string = "StorageArchitect";
  public requiredLevel: number = 4;
  public maxStructures: number = 1;
  public structureType = STRUCTURE_STORAGE;

  public flagConstructions(controller: StructureController): void {
    return;
  }

  public implementConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }

    const spawn = controller.room.find(FIND_MY_SPAWNS)[0];

    const buildableSpaces = RoomUtils.getClosestBuildableArea(spawn);
    if (controller.room.createConstructionSite(buildableSpaces[0], this.structureType) === 0) {
      RoomUtils.createFlag(buildableSpaces[0], FlagType.REQUIRE_ROAD);
      RoomUtils.createFlag(buildableSpaces[0], FlagType.OPEN_CONTAINER);
    }

  }

  public canBuild(controller: StructureController): boolean {
    return controller.level >= this.requiredLevel && this.calculateStructureCount(controller) < this.maxStructures;
  }

  private calculateStructureCount(controller: StructureController): number {
    return controller.room.find(FIND_STRUCTURES).filter((structure) =>
      (structure.structureType as BuildableStructureConstant) === this.structureType).length
      + controller.room.find(FIND_MY_CONSTRUCTION_SITES).filter((structure) =>
        (structure.structureType as BuildableStructureConstant) === this.structureType).length;

  }

  private findOpenSpaceNextToPos(sourcePos: RoomPosition): RoomPosition {
    const buildableSpacesNextToPos = RoomUtils.getAnyBuildableSpacesNextToPos(sourcePos);
    return buildableSpacesNextToPos[0];
  }
}
