import {FlagType} from "../../flag/flag-type";
import {RoomUtils} from "../../room/room-utils";
import {Architect} from "../architect";

export class RoadArchitect implements Architect {
  public name: string = "RoadArchitect";
  public requiredLevel: number = 1;
  public checkRate: number = 20;
  public structureType = STRUCTURE_ROAD;

  public flagConstructions(controller: StructureController): void {
    return;
  }

  public implementConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }

    const spawn = controller.room.find(FIND_MY_SPAWNS)[0];

    this.buildRoadShortest(spawn, controller);

    // Build road access to resources
    const roomMemory = RoomUtils.getRoomMemory(controller.room);
    roomMemory.ownedResources.forEach((resource) => {
      this.buildRoadShortest(spawn, Game.getObjectById(resource.resourceId) as RoomObject);
    });

    // Build road access to collector
    RoomUtils.getAnyBuildableSpacesNextToPos(spawn.pos).forEach((pos) => {
      spawn.room.createConstructionSite(pos, this.structureType);
    });

    // Build road access to POI
    controller.room.find(FIND_FLAGS).filter((flag) => flag.name.indexOf(FlagType.POI) === 0)
      .forEach((flag) => {
        this.buildRoadShortest(spawn, flag);
        flag.remove();
      });

    // Build road access for Object
    controller.room.find(FIND_FLAGS).filter((flag) => flag.name.indexOf(FlagType.REQUIRE_ROAD) === 0)
      .forEach((flag) => {
        RoomUtils.getAnyBuildableSpacesNextToPos(flag.pos).forEach((pos) => {
          spawn.room.createConstructionSite(pos, this.structureType);
        });
        flag.remove();
      });

  }

  public canBuild(controller: StructureController): boolean {
    return controller.my && Game.time % this.checkRate === 0;
  }

  private buildRoadShortest(start: RoomObject, finish: RoomObject): void {
    const path = RoomUtils.getShortestPathToObject(start, finish);
    if (!path.incomplete) {
      path.path.forEach((pos) =>
        start.room!.createConstructionSite(pos, this.structureType));
    }
    finish.room!.createConstructionSite(finish.pos, this.structureType);

  }
}
