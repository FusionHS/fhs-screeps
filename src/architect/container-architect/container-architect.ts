import {FlagType} from "../../flag/flag-type";
import {RoomUtils} from "../../room/room-utils";
import {Architect} from "../architect";

export class ContainerArchitect implements Architect {
  public name: string = "ContainerArchitect";
  public requiredLevel: number = 1;
  public maxStructures: number = 5;
  public structureType = STRUCTURE_CONTAINER;

  public flagConstructions(controller: StructureController): void {
    return;
  }

  public implementConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }
    let itemsBuiltThisTick = 0;
    const spawn = controller.room.find(FIND_MY_SPAWNS)[0];

    // Create Pickup containers for resources
    const roomMemory = RoomUtils.getRoomMemory(controller.room);
    roomMemory.ownedResources.forEach((resource) => {
      if (resource.containerFlagId == null) {
        const resourceObject = (Game.getObjectById(resource.resourceId) as RoomObject);
        const pos = this.findOpenSpaceClosestToPos(spawn, resourceObject);
        if (controller.room.createConstructionSite(pos, this.structureType) === 0) {
          resource.containerFlagId = RoomUtils.createFlag(pos, FlagType.PICKUP);
          RoomUtils.createFlag(pos, FlagType.REQUIRE_ROAD);
          itemsBuiltThisTick++;
        }
      }
    });

    // Create OPEN_CONTAINER containers for resources
    const controllerClosestSpace = this.findOpenSpaceClosestToPos(spawn, controller);
    if (controller.room.createConstructionSite(controllerClosestSpace, this.structureType) === 0) {
      itemsBuiltThisTick++;
      RoomUtils.createFlag(controllerClosestSpace, FlagType.REQUIRE_ROAD);
      RoomUtils.createFlag(controllerClosestSpace, FlagType.OPEN_CONTAINER);
    }

    const remainingBuilds = this.maxStructures - this.calculateStructureCount(controller) - itemsBuiltThisTick;

    // Create OPEN_CONTAINER containers for resources
    if (remainingBuilds > 0) {
      const buildableSpaces = RoomUtils.getClosestBuildableArea(spawn);
      _.times(remainingBuilds, (i) => {
        controller.room.createConstructionSite(
          buildableSpaces[i], this.structureType);
        RoomUtils.createFlag(buildableSpaces[i], FlagType.REQUIRE_ROAD);
        RoomUtils.createFlag(buildableSpaces[i], FlagType.OPEN_CONTAINER);
      });
    }

  }

  public canBuild(controller: StructureController): boolean {
    return controller.level >= this.requiredLevel && this.calculateStructureCount(controller) < this.maxStructures;
  }

  private calculateStructureCount(controller: StructureController): number {
    return controller.room.find(FIND_STRUCTURES).filter((structure) =>
      (structure.structureType as BuildableStructureConstant) === this.structureType).length
      + controller.room.find(FIND_MY_CONSTRUCTION_SITES).filter((structure) =>
        (structure.structureType as BuildableStructureConstant) === this.structureType).length;

  }

  private findOpenSpaceClosestToPos(target: RoomObject, from: RoomObject): RoomPosition {
    const path = RoomUtils.getShortestPathToObject(target, from);
    return path.path.pop()!;
  }

  private findOpenSpaceNextToPos(sourcePos: RoomPosition): RoomPosition {
    const buildableSpacesNextToPos = RoomUtils.getAnyBuildableSpacesNextToPos(sourcePos);
    return buildableSpacesNextToPos[0];
  }
}
