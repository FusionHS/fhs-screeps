import {Architect} from "./architect";
import {ContainerArchitect} from "./container-architect/container-architect";
import {ExtensionArchitect} from "./extension-architect/extension-architect";
import {RampartArchitect} from "./rampart-architect/rampart-architect";
import {RoadArchitect} from "./road-architect/road-architect";
import {TowerArchitect} from "./tower-architect/tower-architect";
import {WallArchitect} from "./wall-architect/wall-architect";
import {StorageArchitect} from "./storage-architect/storage-architect";

export class ArchitectManager {
  private wallArchitect: Architect = new WallArchitect();
  private containerArchitect: Architect = new ContainerArchitect();
  private extensionArchitect: Architect = new ExtensionArchitect();
  private rampartArchitect: Architect = new RampartArchitect();
  private roadArchitect: Architect = new RoadArchitect();
  private towerArchitect: Architect = new TowerArchitect();
  private storageArchitect: Architect = new StorageArchitect();

  public flagConstructions(controller: StructureController) {
    this.containerArchitect.flagConstructions(controller);
    this.extensionArchitect.flagConstructions(controller);
    this.towerArchitect.flagConstructions(controller);
    this.storageArchitect.flagConstructions(controller);
    this.rampartArchitect.flagConstructions(controller);
    this.wallArchitect.flagConstructions(controller);
    this.roadArchitect.flagConstructions(controller);
  }

  public implementConstructions(controller: StructureController) {
    this.containerArchitect.implementConstructions(controller);
    this.extensionArchitect.implementConstructions(controller);
    this.towerArchitect.implementConstructions(controller);
    this.storageArchitect.implementConstructions(controller);
    this.rampartArchitect.implementConstructions(controller);
    this.wallArchitect.implementConstructions(controller);
    this.roadArchitect.implementConstructions(controller);
  }
}
