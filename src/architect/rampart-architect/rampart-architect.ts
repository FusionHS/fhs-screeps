import {Architect} from "../architect";
import {FlagType} from "../../flag/flag-type";

export class RampartArchitect implements Architect {
  public name: string = "RampartArchitect";
  public requiredLevel: number = 2;
  public structureType = STRUCTURE_RAMPART;

  public flagConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }
  }

  public implementConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }

    controller.room.find(FIND_FLAGS).filter((flag) => flag.name.indexOf(FlagType.BUILD_RAMPARTS) === 0)
      .forEach((flag) => {
        controller.room.createConstructionSite(flag.pos, this.structureType);
        flag.remove();
      });

  }

  public canBuild(controller: StructureController): boolean {
    return controller.level >= this.requiredLevel;
  }
}
