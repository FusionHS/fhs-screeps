export interface Architect {
  name: string;

  flagConstructions(controller: StructureController): void;

  implementConstructions(controller: StructureController): void;
}
