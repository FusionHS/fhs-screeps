import {FlagType} from "../../flag/flag-type";
import {RoomUtils} from "../../room/room-utils";
import {Architect} from "../architect";

export class TowerArchitect implements Architect {
  public name: string = "TowerArchitect";
  public structureType = STRUCTURE_TOWER;
  public requiredLevel: number = 3;
  private availableStructures: { [key: string]: number; } = {
    3: 1,
    4: 1,
    5: 2,
    6: 2,
    7: 3,
    8: 6
  };

  public flagConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }
  }

  public implementConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }

    let itemsBuiltThisTick = 0;
    const spawn = controller.room.find(FIND_MY_SPAWNS)[0];

    const buildableSpaces = RoomUtils.getClosestBuildableArea(spawn);

    const remainingBuilds = this.getAvailableStructures(controller.level) - this.calculateStructureCount(controller);

    buildableSpaces.forEach((buildableSpace) => {
      if (remainingBuilds > itemsBuiltThisTick && controller.room.createConstructionSite(buildableSpace, this.structureType) === 0) {
        RoomUtils.createFlag(buildableSpace, FlagType.REQUIRE_ROAD);
        itemsBuiltThisTick++;
      }
    });
  }

  public canBuild(controller: StructureController): boolean {
    return controller.level >= this.requiredLevel && this.calculateStructureCount(controller) < this.getAvailableStructures(controller.level);
  }

  private calculateStructureCount(controller: StructureController): number {
    return controller.room.find(FIND_MY_STRUCTURES).filter((structure) =>
      (structure.structureType as BuildableStructureConstant) === this.structureType).length
      + controller.room.find(FIND_MY_CONSTRUCTION_SITES).filter((structure) =>
        (structure.structureType as BuildableStructureConstant) === this.structureType).length;

  }

  private getAvailableStructures(level: number) {
    const available = this.availableStructures[level];
    return available !== undefined ? available : 0;
  }
}
