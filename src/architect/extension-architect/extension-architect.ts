import {FlagType} from "../../flag/flag-type";
import {RoomUtils} from "../../room/room-utils";
import {Architect} from "../architect";

export class ExtensionArchitect implements Architect {

  public name: string = "ExtensionArchitect";
  public structureType = STRUCTURE_EXTENSION;
  private requiredLevel: number = 2;
  private availableStructures: { [key: string]: number; } = {
    2: 5,
    3: 10,
    4: 20,
    5: 30,
    6: 40,
    7: 50,
    8: 60
  };

  public flagConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }
  }

  public implementConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }

    let itemsBuiltThisTick = 0;
    const spawn = controller.room.find(FIND_MY_SPAWNS)[0];

    const buildableSpaces = RoomUtils.getClosestBuildableArea(spawn);

    const remainingBuilds = this.getAvailableStructures(controller.level) - this.calculateStructureCount(controller);

    buildableSpaces.forEach((buildableSpace) => {
      if (remainingBuilds > itemsBuiltThisTick && controller.room.createConstructionSite(buildableSpace, this.structureType) === 0) {
        RoomUtils.createFlag(buildableSpace, FlagType.REQUIRE_ROAD);
        itemsBuiltThisTick++;
      }
    });

  }

  public canBuild(controller: StructureController): boolean {
    return controller.level >= this.requiredLevel && this.calculateStructureCount(controller) < this.getAvailableStructures(controller.level);
  }

  private calculateStructureCount(controller: StructureController): number {
    return controller.room.find(FIND_MY_STRUCTURES).filter((structure) =>
      (structure.structureType as BuildableStructureConstant) === this.structureType).length
      + controller.room.find(FIND_MY_CONSTRUCTION_SITES).filter((structure) =>
        (structure.structureType as BuildableStructureConstant) === this.structureType).length;

  }

  private getAvailableStructures(level: number) {
    const available = this.availableStructures[level];
    return available !== undefined ? available : 0;
  }
}
