import {FlagType} from "../../flag/flag-type";
import {RoomUtils} from "../../room/room-utils";
import {Architect} from "../architect";

export class WallArchitect implements Architect {
  public name: string = "WallArchitect";
  public requiredLevel: number = 4;
  private checkRate: number = 100;
  private structureType = STRUCTURE_WALL;

  private exits = [
    FIND_EXIT_TOP,
    FIND_EXIT_RIGHT,
    FIND_EXIT_BOTTOM,
    FIND_EXIT_LEFT
  ];

  public flagConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }
  }

  public implementConstructions(controller: StructureController): void {
    if (!this.canBuild(controller)) {
      return;
    }

    // if (RoomUtils.getRoomMemory(controller.room).defenceWallsBuilt) {
    //   return;
    // }

    this.exits.forEach((exit) => {
      const exitPoints = controller.room.find(exit);

      if (exitPoints.length === 0) {
        return;
      }

      this.roundOffWalls(exitPoints, exit);

      exitPoints.forEach((point, index) => {
        let buildPos = null;
        switch (exit) {
          case FIND_EXIT_TOP:
            buildPos = new RoomPosition(point.x, point.y + 2, point.roomName);
            break;
          case FIND_EXIT_RIGHT:
            buildPos = new RoomPosition(point.x - 2, point.y, point.roomName);
            break;
          case FIND_EXIT_BOTTOM:
            buildPos = new RoomPosition(point.x, point.y - 2, point.roomName);
            break;
          case FIND_EXIT_LEFT:
            buildPos = new RoomPosition(point.x + 2, point.y, point.roomName);
            break;
          default:
            throw new Error(`Not acceptable value for exit: ${exit}`);
        }

        if (buildPos.look().filter((obj) => obj.type === LOOK_STRUCTURES
          || obj.type === LOOK_CONSTRUCTION_SITES
          || obj.type === LOOK_FLAGS).length > 0) {
          return;
        }

        if (index === Math.floor((exitPoints.length - 1) / 2)
          || index === Math.floor((exitPoints.length - 1) / 2) + 1) {
          RoomUtils.createFlag(buildPos, FlagType.BUILD_RAMPARTS);
          RoomUtils.createFlag(point, FlagType.POI);
        } else {
          controller.room.createConstructionSite(buildPos, this.structureType);
        }
      });

    });

    RoomUtils.getRoomMemory(controller.room).defenceWallsBuilt = true;

  }

  public canBuild(controller: StructureController): boolean {
    return controller.level >= this.requiredLevel && Game.time % this.checkRate === 0;
  }

  private roundOffWalls(exitPoints: Array<FindTypes[1 | 3 | 5 | 7]>, exit: 1 | 3 | 5 | 7) {
    let lastChecked = new RoomPosition(25, 25, exitPoints[0].roomName);

    exitPoints.forEach((point, index) => {

      if (index === 0) {
        this.buildWallBack(point, exit);
      } else if (index === exitPoints.length - 1) {
        this.buildWallFront(point, exit);
      } else if (lastChecked.isNearTo(point)) {
        lastChecked = point;
        return;
      } else {
        this.buildWallBack(point, exit);
        this.buildWallFront(exitPoints[index - 1], exit);
      }

      lastChecked = point;

    });
  }

  private buildWallBack(point: RoomPosition, exit: 1 | 3 | 5 | 7) {
    const room = Game.rooms[point.roomName];
    switch (exit) {
      case FIND_EXIT_TOP:
        room.createConstructionSite(point.x - 1, point.y + 2, this.structureType);
        room.createConstructionSite(point.x - 2, point.y + 2, this.structureType);
        room.createConstructionSite(point.x - 2, point.y + 1, this.structureType);
        break;
      case FIND_EXIT_RIGHT:
        room.createConstructionSite(point.x - 2, point.y + 1, this.structureType);
        room.createConstructionSite(point.x - 2, point.y + 2, this.structureType);
        room.createConstructionSite(point.x - 1, point.y + 2, this.structureType);
        break;
      case FIND_EXIT_BOTTOM:
        room.createConstructionSite(point.x - 1, point.y - 2, this.structureType);
        room.createConstructionSite(point.x - 2, point.y - 2, this.structureType);
        room.createConstructionSite(point.x - 2, point.y - 1, this.structureType);
        break;
      case FIND_EXIT_LEFT:
        room.createConstructionSite(point.x + 2, point.y - 1, this.structureType);
        room.createConstructionSite(point.x + 2, point.y - 2, this.structureType);
        room.createConstructionSite(point.x + 1, point.y - 2, this.structureType);
        break;
    }
  }

  private buildWallFront(point: RoomPosition, exit: 1 | 3 | 5 | 7) {
    const room = Game.rooms[point.roomName];
    switch (exit) {
      case FIND_EXIT_TOP:
        room.createConstructionSite(point.x + 1, point.y + 2, this.structureType);
        room.createConstructionSite(point.x + 2, point.y + 2, this.structureType);
        room.createConstructionSite(point.x + 2, point.y + 1, this.structureType);
        break;
      case FIND_EXIT_RIGHT:
        room.createConstructionSite(point.x - 2, point.y - 1, this.structureType);
        room.createConstructionSite(point.x - 2, point.y - 2, this.structureType);
        room.createConstructionSite(point.x - 1, point.y - 2, this.structureType);
        break;
      case FIND_EXIT_BOTTOM:
        room.createConstructionSite(point.x + 1, point.y - 2, this.structureType);
        room.createConstructionSite(point.x + 2, point.y - 2, this.structureType);
        room.createConstructionSite(point.x + 2, point.y - 1, this.structureType);
        break;
      case FIND_EXIT_LEFT:
        room.createConstructionSite(point.x + 2, point.y + 1, this.structureType);
        room.createConstructionSite(point.x + 2, point.y + 2, this.structureType);
        room.createConstructionSite(point.x + 1, point.y + 2, this.structureType);
        break;
    }
  }

}
