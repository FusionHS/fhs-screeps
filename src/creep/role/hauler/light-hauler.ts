import {Hauler} from "./hauler";

export class LightHauler extends Hauler {
  public role: string = "Hauler";
  public name: string = "LightHauler";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, MOVE, CARRY, CARRY, CARRY, CARRY];
  }
}
