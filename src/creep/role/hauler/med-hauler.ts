import {Hauler} from "./hauler";

export class MedHauler extends Hauler {
  public role: string = "Hauler";
  public name: string = "MedHauler";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, CARRY, CARRY, CARRY];
  }
}
