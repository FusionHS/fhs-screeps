import {Builder} from "./builder";

export class LightBuilder extends Builder {
  public role: string = "Builder";
  public name: string = "LightBuilder";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, MOVE, CARRY, CARRY, WORK];
  }
}
