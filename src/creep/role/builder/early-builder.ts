import {Builder} from "./builder";

export class EarlyBuilder extends Builder {
  public role: string = "Builder";
  public name: string = "EarlyBuilder";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, MOVE, CARRY, CARRY, WORK];
  }
}
