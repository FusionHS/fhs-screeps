import {Builder} from "./builder";

export class ImprovedBuilder extends Builder {
  public role: string = "Builder";
  public name: string = "ImprovedBuilder";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, MOVE, MOVE, CARRY, CARRY, WORK, WORK, WORK];
  }
}
