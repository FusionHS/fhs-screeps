import {Miner} from "./miner";

export class LightMiner extends Miner {
  public role: string = "Miner";
  public name: string = "LightMiner";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, WORK, WORK];
  }

}
