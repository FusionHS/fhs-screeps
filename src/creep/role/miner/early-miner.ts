import {Miner} from "./miner";

export class EarlyMiner extends Miner {
  public role: string = "Hauler";
  public name: string = "EarlyMiner";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, MOVE, CARRY, CARRY, WORK];
  }
}
