import {Miner} from "./miner";

export class FullMiner extends Miner {
  public role: string = "Miner";
  public name: string = "FullMiner";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, WORK, WORK, WORK, WORK, WORK];
  }
}
