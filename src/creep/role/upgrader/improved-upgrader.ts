import {Upgrader} from "./upgrader";

export class ImprovedUpgrader extends Upgrader {
  public role: string = "Upgrader";
  public name: string = "ImprovedUpgrader";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, CARRY, CARRY, WORK, WORK, WORK, WORK];
  }

}
