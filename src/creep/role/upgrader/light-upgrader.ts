import {Upgrader} from "./upgrader";

export class LightUpgrader extends Upgrader {
  public role: string = "Upgrader";
  public name: string = "LightUpgrader";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, CARRY, WORK, WORK];
  }

}
