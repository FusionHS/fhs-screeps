import {Upgrader} from "./upgrader";

export class EarlyUpgrader extends Upgrader {
  public role: string = "Upgrader";
  public name: string = "EarlyUpgrader";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, CARRY, WORK, WORK];
  }

}
