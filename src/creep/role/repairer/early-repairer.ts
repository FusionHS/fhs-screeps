import {Repairer} from "./repairer";

export class EarlyRepairer extends Repairer {
  public role: string = "Repairer";
  public name: string = "EarlyRepairer";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, CARRY, CARRY, CARRY, WORK];
  }
}
