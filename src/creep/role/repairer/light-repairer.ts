import {Repairer} from "./repairer";

export class LightRepairer extends Repairer {
  public role: string = "Repairer";
  public name: string = "LightRepairer";

  constructor(public roleLimit: number) {
    super(roleLimit);
  }

  public getParts(): BodyPartConstant[] {
    return [MOVE, MOVE, CARRY, CARRY, WORK];
  }
}
