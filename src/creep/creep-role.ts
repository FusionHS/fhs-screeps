import {CreepMemoryContainer} from "../memory/creep-memory-container";
import {CreepUtils} from "./creep-utils";

export abstract class CreepRole {

  public abstract role: string;
  public abstract name: string;
  private partCosts: { [part: string]: number; } = {
    move: 50,
    work: 100,
    carry: 50,
    attack: 80,
    ranged_attack: 150,
    tough: 10,
    heal: 250,
    claim: 600
  };

  constructor(public roleLimit: number) {
  }

  public getDefaultMemory(room: Room, localAgent: boolean): CreepMemoryContainer {
    return CreepUtils.getDefaultMemory(this, room, localAgent);
  }

  public generateName(): string {
    return this.name + Game.time;
  }

  public getName(): string {
    return this.name;
  }

  public abstract getParts(): BodyPartConstant[];

  public getRequiredEnergy(): number {
    let cost = 0;
    this.getParts().forEach((part) => cost += this.partCosts[part]);
    return cost;
  }

  public getRole(): string {
    return this.role;
  }
}
