import {FlagType} from "../flag/flag-type";
import {CreepMemoryContainer} from "../memory/creep-memory-container";
import {ResourceMemoryContainer} from "../memory/resource-memory-container";
import {RoomUtils} from "../room/room-utils";
import {CreepAction} from "./creep-action";
import {CreepRole} from "./creep-role";

export class CreepUtils {

  public static getMemory(creep: Creep): CreepMemoryContainer {
    return creep.memory as CreepMemoryContainer;
  }

  public static getAction(creep: Creep): CreepAction {
    return (creep.memory as CreepMemoryContainer).actionMemory.action;
  }

  public static setAction(creep: Creep, action: CreepAction): void {
    (creep.memory as CreepMemoryContainer).actionMemory.action = action;
  }

  public static spawnLocalCreep(spawner: StructureSpawn, creep: CreepRole) {
    spawner.spawnCreep(
      creep.getParts(),
      creep.generateName(),
      {memory: creep.getDefaultMemory(spawner.room, true)});
  }

  public static harvest(creep: Creep, source: Source) {
    this.getMemory(creep).actionMemory.targetId = source.id;
    if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
      creep.moveTo(source, {visualizePathStyle: {stroke: "#ffaa00"}});
    }
  }

  public static offLoad(creep: Creep, structure: Structure) {
    this.getMemory(creep).actionMemory.targetId = structure.id;
    if (creep.transfer(structure, "energy") === ERR_NOT_IN_RANGE) {
      creep.moveTo(structure, {visualizePathStyle: {stroke: "#ffaa00"}});
    }
  }

  public static upgrade(creep: Creep, controller: StructureController) {
    this.getMemory(creep).actionMemory.targetId = controller.id;
    if (creep.upgradeController(controller) === ERR_NOT_IN_RANGE) {
      creep.moveTo(controller, {visualizePathStyle: {stroke: "#ffaa00"}});
    }
  }

  public static build(creep: Creep, buildSite: ConstructionSite) {
    this.getMemory(creep).actionMemory.targetId = buildSite.id;
    if (creep.build(buildSite) === ERR_NOT_IN_RANGE) {
      creep.moveTo(buildSite, {visualizePathStyle: {stroke: "#00ff00"}});
    }
  }

  public static repair(creep: Creep, structure: Structure) {
    this.getMemory(creep).actionMemory.targetId = structure.id;
    if (creep.repair(structure) === ERR_NOT_IN_RANGE) {
      creep.moveTo(structure, {visualizePathStyle: {stroke: "#0000ff"}});
    }

  }

  public static getAvailableSource(creep: Creep): Source | null {

    const roomMemory = RoomUtils.getRoomMemory(creep.room);

    for (const key in roomMemory.ownedResources.filter(((resource) => resource.resourceType === LOOK_SOURCES))) {
      const source = roomMemory.ownedResources[key];
      if (this.isSourceSpotAvailable(source)) {
        return Game.getObjectById(source.resourceId);
      }
    }
    return null;
  }

  public static offLoadEnergy(creep: Creep) {

    if (this.getMemory(creep).actionMemory.targetId !== undefined) {
      const structure = Game.getObjectById(this.getMemory(creep).actionMemory.targetId) as StructureContainer | StructureExtension | StructureTower | StructureSpawn | StructureStorage | Resource;
      if ((structure instanceof StructureContainer && structure.store.energy === structure.storeCapacity)
        || (structure instanceof StructureExtension && structure.energy === structure.energyCapacity)
        || (structure instanceof StructureStorage && structure.store.energy === structure.storeCapacity)
        || (structure instanceof StructureSpawn && structure.energy === structure.energyCapacity)
        || (structure instanceof StructureTower && structure.energy === structure.energyCapacity)
        || (structure instanceof Resource)) {
        // Keep going
      } else {
        this.offLoad(creep, structure);
        return;
      }
    }

    const target = creep.room.find(FIND_MY_STRUCTURES).filter((structure) =>
      (structure.structureType === STRUCTURE_EXTENSION
        || structure.structureType === STRUCTURE_SPAWN
        || structure.structureType === STRUCTURE_TOWER)
      && structure.energy < structure.energyCapacity
    );

    const containers = creep.room.find(FIND_FLAGS)
      .filter((flag) => flag.name.indexOf(FlagType.OPEN_CONTAINER) === 0 && this.containerIsCompleteAtFlag(flag))
      .map((flag) => this.getContainerAtFlag(flag))
      .filter((container) => container.store.energy < container.storeCapacity)
      .sort((a, b) => a.store.energy - b.store.energy);

    if (target[0] != null) {
      this.getMemory(creep).actionMemory.action = CreepAction.OFFLOADING;
      this.getMemory(creep).actionMemory.targetId = target[0].id;
      this.offLoad(creep, target[0]);
    } else if (containers.length > 0) {
      this.getMemory(creep).actionMemory.action = CreepAction.OFFLOADING;
      this.getMemory(creep).actionMemory.targetId = containers[0].id;
      this.offLoad(creep, containers[0]);
    } else {
      this.getMemory(creep).actionMemory.action = CreepAction.IDLE;
      this.getMemory(creep).actionMemory.targetId = undefined;
    }
  }

  public static idleAtSpawn(creep: Creep) {
    creep.moveTo(creep.room.find(FIND_MY_SPAWNS)[0]);
  }

  public static mineEnergy(creep: Creep) {
    this.gatherEnergy(creep, CreepAction.MINING);
  }

  public static refillEnergy(creep: Creep) {
    this.gatherEnergy(creep, CreepAction.REFILLING);
  }

  public static assignDedicatedSource(creep: Creep) {
    const availableDedicatedSource = this.getAvailableDedicatedSource(creep);

    const memory = this.getMemory(creep);

    if (availableDedicatedSource !== null) {
      memory.actionMemory.action = CreepAction.POSITIONING;
      memory.actionMemory.targetId = availableDedicatedSource.resourceId;
    } else {
      memory.actionMemory.action = CreepAction.RECYCLING;
      memory.actionMemory.targetId = undefined;
    }
  }

  public static getAvailableDedicatedSource(creep: Creep): ResourceMemoryContainer | null {

    const roomMemory = RoomUtils.getRoomMemory(creep.room);

    for (const key in roomMemory.ownedResources.filter(((resource) => resource.resourceType === LOOK_SOURCES))) {
      const source = roomMemory.ownedResources[key];
      if (this.isDedicatedSourceSpotAvailable(source)) {
        return source;
      }
    }
    return null;
  }

  public static moveToAssignedResourceContainer(creep: Creep) {

    const creepMemory = this.getMemory(creep);
    const resourceMemoryContainer = RoomUtils.getRoomMemory(creep.room).ownedResources
      .filter((resource) => resource.resourceId === creepMemory.actionMemory.targetId)[0];

    const container = creep.room.find(FIND_FLAGS).filter((flag) => flag.name === resourceMemoryContainer.containerFlagId)[0];

    if (creep.pos.x === container.pos.x && creep.pos.y === container.pos.y) {
      creepMemory.actionMemory.targetId = resourceMemoryContainer.resourceId;
      creepMemory.actionMemory.action = CreepAction.MINING;

    } else {
      this.MoveCreepOnTo(creep, container.pos);
    }

  }

  public static mineAdjacentSource(creep: Creep) {
    const targetId = this.getMemory(creep).actionMemory.targetId;
    if (creep.harvest(Game.getObjectById(targetId) as Source) === ERR_NOT_IN_RANGE) {
      this.getMemory(creep).actionMemory.action = CreepAction.IDLE;
      this.getMemory(creep).actionMemory.targetId = undefined;
    }

  }

  public static assignNonStoredEnergyForCollection(creep: Creep): boolean {
    const droppedEnergy = creep.room.find(FIND_DROPPED_RESOURCES)
      .filter((resource) => resource.resourceType === "energy" && resource.amount >= creep.carryCapacity)
      .filter((resource) => !this.isTargetAssigned(resource))
      .sort((a, b) => b.amount - a.amount);

    if (droppedEnergy.length > 0) {
      this.getMemory(creep).actionMemory.action = CreepAction.CLEANING;
      this.getMemory(creep).actionMemory.targetId = droppedEnergy[0].id;
      return true;
    }
    return false;

  }

  public static assignPickupContainerEnergy(creep: Creep) {

    const containers = creep.room.find(FIND_FLAGS)
      .filter((flag) => flag.name.indexOf(FlagType.PICKUP) === 0 && this.containerIsCompleteAtFlag(flag))
      .map((flag) => this.getContainerAtFlag(flag))
      .filter((container) => !this.isTargetAssigned(container) && container.store.energy > 0)
      .sort((a, b) => b.store.energy - a.store.energy);

    if (containers.length > 0) {
      this.getMemory(creep).actionMemory.action = CreepAction.COLLECTING;
      this.getMemory(creep).actionMemory.targetId = containers[0].id;

      return true;
    }

    return false;
  }

  public static moveToTargetAndCollect(creep: Creep) {
    const target = Game.getObjectById(this.getMemory(creep).actionMemory.targetId) as Resource | StructureContainer;

    if (target === null || target instanceof StructureContainer && target.store.energy <= 0) {
      this.setAction(creep, CreepAction.IDLE);
      this.getMemory(creep).actionMemory.targetId = undefined;
      return;
    }

    if ((target instanceof Resource && creep.pickup(target) === ERR_NOT_IN_RANGE)
      || (target instanceof StructureContainer && creep.withdraw(target, "energy") === ERR_NOT_IN_RANGE)) {
      creep.moveTo(target, {visualizePathStyle: {stroke: "#0000ff"}});
    }
  }

  public static attemptStoreInAdjacentContainer(creep: Creep): boolean {
    const containers = creep.pos.findInRange(FIND_MY_STRUCTURES, 1)
      .filter((structure) => (structure.structureType as BuildableStructureConstant) === STRUCTURE_CONTAINER);

    if (containers.length > 0) {
      const structureContainer = Game.getObjectById(containers[0].id) as StructureContainer;

      if (structureContainer.store.energy < structureContainer.storeCapacity) {
        this.offLoad(creep, structureContainer);
        return true;
      }

    }
    return false;
  }

  public static collectEnergyFromClosestDroppedPile(creep: Creep): boolean {

    const droppedEnergy = creep.room.find(FIND_DROPPED_RESOURCES)
      .filter((resource) => resource.resourceType === "energy" && resource.amount >= creep.carryCapacity)
      .sort((a, b) => RoomUtils.getMovementCost(creep.pos, a.pos) - RoomUtils.getMovementCost(creep.pos, b.pos));

    if (droppedEnergy.length > 0) {
      this.getMemory(creep).actionMemory.action = CreepAction.REFILLING;
      // this.getMemory(creep).actionMemory.targetId = droppedEnergy[0].id;
      this.collectEnergy(creep, droppedEnergy[0]);
      return true;
    }
    return false;
  }

  public static collectEnergyFromClosestContainer(creep: Creep): boolean {

    const closestContainers = creep.room.find(FIND_STRUCTURES)
      .filter((structure) => (structure.structureType as BuildableStructureConstant) === STRUCTURE_CONTAINER
        || (structure.structureType as BuildableStructureConstant) === STRUCTURE_STORAGE)
      .map((container) => Game.getObjectById(container.id) as StructureContainer | StructureStorage)
      .filter((container) => container.store.energy >= creep.carryCapacity)
      .sort((a, b) => RoomUtils.getMovementCost(creep.pos, a.pos) - RoomUtils.getMovementCost(creep.pos, b.pos));

    if (closestContainers.length > 0) {
      this.getMemory(creep).actionMemory.action = CreepAction.REFILLING;
      // this.getMemory(creep).actionMemory.targetId = droppedEnergy[0].id;
      this.collectEnergy(creep, closestContainers[0]);
      return true;
    }
    return false;
  }

  public static getDefaultMemory(creep: CreepRole, room: Room, localAgent: boolean): CreepMemoryContainer {
    return new CreepMemoryContainer(
      creep.getRole().valueOf(),
      creep.getName().valueOf(),
      room.name.valueOf(),
      localAgent,
      {action: CreepAction.IDLE});
  }

  private static gatherEnergy(creep: Creep, successAction: CreepAction) {
    const memoryContainer = this.getMemory(creep);

    if (memoryContainer.actionMemory.action === successAction) {
      this.harvest(creep, Game.getObjectById(memoryContainer.actionMemory.targetId)! as Source);
      return;
    }

    const availableSource = this.getAvailableSource(creep);

    if (availableSource == null) {
      memoryContainer.actionMemory.action = CreepAction.IDLE;
      memoryContainer.actionMemory.targetId = undefined;
    } else {
      memoryContainer.actionMemory.action = successAction;
      this.harvest(creep, availableSource);
    }
  }

  private static isSourceSpotAvailable(source: ResourceMemoryContainer): boolean {
    return _.filter(Game.creeps, (creep) =>
      this.getMemory(creep).actionMemory.targetId === source.resourceId).length < source.availableSpots;
  }

  private static isDedicatedSourceSpotAvailable(source: ResourceMemoryContainer): boolean {
    return _.filter(Game.creeps, (creep) =>
      this.getMemory(creep).actionMemory.targetId === source.resourceId).length < 1;
  }

  private static MoveCreepOnTo(creep: Creep, pos: RoomPosition) {
    creep.moveTo(pos, {visualizePathStyle: {stroke: "#ffaa00"}});
  }

  private static isTargetAssigned(resource: Resource | StructureContainer | StructureStorage) {
    return _.filter(Game.creeps, (creep) =>
      this.getMemory(creep).actionMemory.targetId === resource.id).length > 0;
  }

  private static getContainerAtFlag(flag: Flag): StructureContainer | StructureStorage {
    return flag.pos.lookFor(LOOK_STRUCTURES)
      .filter((structure) => structure.structureType === STRUCTURE_CONTAINER || structure.structureType === STRUCTURE_STORAGE)[0] as StructureContainer | StructureStorage;

  }

  private static containerIsCompleteAtFlag(flag: Flag): boolean {
    return flag.pos.lookFor(LOOK_STRUCTURES)
      .filter((structure) => structure.structureType === STRUCTURE_CONTAINER || structure.structureType === STRUCTURE_STORAGE).length > 0;
  }

  private static collectEnergy(creep: Creep, target: Resource | StructureContainer | StructureStorage) {
    if ((target instanceof Resource && creep.pickup(target) === ERR_NOT_IN_RANGE)
      || ((target instanceof StructureContainer || target instanceof StructureStorage) && creep.withdraw(target, "energy") === ERR_NOT_IN_RANGE)) {
      creep.moveTo(target, {visualizePathStyle: {stroke: "#0000ff"}});
    }
  }
}
