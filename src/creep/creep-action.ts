export enum CreepAction {
  IDLE, // 0
  REFILLING, // 1
  BUILDING, // 2
  UPGRADING, // 3
  REPAIRING, // 4
  MINING, // 5
  OFFLOADING, // 6
  POSITIONING, // 7
  RECYCLING, // 8
  CLEANING, // 9
  COLLECTING, // 10
  RENEWING, // 11
  SPAWNING // 12
}
