import {RoomUtils} from "../../room/room-utils";
import {StarterGovernor} from "./govenor-role/starter-governor";
import {TierThreeGovernor} from "./govenor-role/tier-three-governor";
import {TierTwoGovernor} from "./govenor-role/tier-two-governor";
import {GovernorRole} from "./governor-role";

export class GovernorFactory {

  private static energyBreakPoints: { [key: string]: number; } = {
    1: 300,           // 300
    2: 300 + 5 * 50,  // 550
    3: 300 + 10 * 50, // 800
    4: 300 + 20 * 50, // 1300
    5: 300 + 30 * 50, // 1800
    6: 300 + 40 * 50, // 2300
    7: 300 + 50 * 50, // 2800
    8: 300 + 60 * 50  // 3300
  };

  public static getGovernor(room: Room): GovernorRole {

    const roomMemory = RoomUtils.getRoomMemory(room);
    const sources = roomMemory.ownedResources.filter((resource) => resource.resourceType === LOOK_SOURCES);
    const constructionsSites = room.find(FIND_MY_CONSTRUCTION_SITES).length;

    if (room.energyCapacityAvailable <= this.energyBreakPoints[1]) {
      return new StarterGovernor(room, sources, constructionsSites);
    }
    if (room.energyCapacityAvailable <= this.energyBreakPoints[2]) {
      return new TierTwoGovernor(room, sources, constructionsSites);
    }
    if (room.energyCapacityAvailable <= this.energyBreakPoints[3]) {
      return new TierThreeGovernor(room, sources, constructionsSites);
    }
    if (room.energyCapacityAvailable <= this.energyBreakPoints[4]) {
      return new TierThreeGovernor(room, sources, constructionsSites);
    }
    if (room.energyCapacityAvailable <= this.energyBreakPoints[5]) {
      return new TierThreeGovernor(room, sources, constructionsSites);
    }
    if (room.energyCapacityAvailable <= this.energyBreakPoints[6]) {
      return new TierThreeGovernor(room, sources, constructionsSites);
    }
    if (room.energyCapacityAvailable <= this.energyBreakPoints[7]) {
      return new TierThreeGovernor(room, sources, constructionsSites);
    }
    if (room.energyCapacityAvailable <= this.energyBreakPoints[8]) {
      return new TierThreeGovernor(room, sources, constructionsSites);
    }
    return new StarterGovernor(room, sources, constructionsSites);
  }
}
