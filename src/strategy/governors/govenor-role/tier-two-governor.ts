import {ArchitectManager} from "../../../architect/architect-manager";
import {CreepRole} from "../../../creep/creep-role";
import {CreepUtils} from "../../../creep/creep-utils";
import {Builder} from "../../../creep/role/builder/builder";
import {ImprovedBuilder} from "../../../creep/role/builder/improved-builder";
import {Hauler} from "../../../creep/role/hauler/hauler";
import {LightHauler} from "../../../creep/role/hauler/light-hauler";
import {MedHauler} from "../../../creep/role/hauler/med-hauler";
import {FullMiner} from "../../../creep/role/miner/full-miner";
import {LightMiner} from "../../../creep/role/miner/light-miner";
import {Miner} from "../../../creep/role/miner/miner";
import {LightRepairer} from "../../../creep/role/repairer/light-repairer";
import {Repairer} from "../../../creep/role/repairer/repairer";
import {ImprovedUpgrader} from "../../../creep/role/upgrader/improved-upgrader";
import {Upgrader} from "../../../creep/role/upgrader/upgrader";
import {ResourceMemoryContainer} from "../../../memory/resource-memory-container";
import {GovernorRole} from "../governor-role";
import {StarterGovernor} from "./starter-governor";

export class TierTwoGovernor implements GovernorRole {

  private defaultMiner: Miner;
  private defaultUpgrader: Upgrader;
  private defaultRepairer: Repairer;
  private defaultBuilder: Builder;
  private defaultHauler: Hauler;
  private architectManager: ArchitectManager = new ArchitectManager();
  private fallbackStrategy: GovernorRole;

  private spawning: boolean = false;
  private room: Room;

  constructor(room: Room, sources: ResourceMemoryContainer[], constructionsSites: number) {
    this.room = room;

    this.defaultMiner = new FullMiner(sources.length);
    this.defaultHauler = new MedHauler(sources.length + 1);
    this.defaultUpgrader = new ImprovedUpgrader(2);
    this.defaultRepairer = new LightRepairer(1);
    this.defaultBuilder = new ImprovedBuilder(constructionsSites > 0 ? 3 : 0);

    this.fallbackStrategy = new StarterGovernor(room, sources, constructionsSites);

  }

  public spawnCreeps(): void {
    this.orderTowers();

    const miners = _.filter(Game.creeps, (creep) => CreepUtils.getMemory(creep).role === this.defaultMiner.getRole());
    const haulers = _.filter(Game.creeps, (creep) => CreepUtils.getMemory(creep).role === this.defaultHauler.getRole());

    if (miners.length === 0 && this.room.energyAvailable <= 300) {
      this.spawnCreepsByRole(new LightMiner(1));
    } else if (miners.length === 0) {
      this.spawnCreepsByRole(this.defaultMiner);
    } else if (haulers.length === 0 && this.room.energyAvailable <= 300) {
      this.spawnCreepsByRole(new LightHauler(1));
    } else if (haulers.length === 0) {
      this.spawnCreepsByRole(this.defaultHauler);
    } else {
      this.spawnCreepsByRole(this.defaultHauler);
      this.spawnCreepsByRole(this.defaultMiner);
      this.spawnCreepsByRole(this.defaultBuilder);
      this.spawnCreepsByRole(this.defaultUpgrader);
      this.spawnCreepsByRole(this.defaultRepairer);

    }

  }

  public orderCreep(creep: Creep): void {

    switch (CreepUtils.getMemory(creep).role) {
      case "Miner":
        this.orderMiner(creep);
        break;
      case "Hauler":
        this.orderHauler(creep);
        break;
      case "Builder":
        this.orderBuilder(creep);
        break;
      case "Repairer":
        this.orderRepairer(creep);
        break;
      case "Upgrader":
        this.orderUpgrader(creep);
        break;
      default:
        this.fallbackStrategy.orderCreep(creep);
        break;

    }
  }

  public construction(): void {
    const rooms = _.filter(Game.rooms, (room) => room.controller != null);
    rooms.forEach((room) => this.architectManager.flagConstructions(room.controller!));
    rooms.forEach((room) => this.architectManager.implementConstructions(room.controller!));
  }

  public orderTowers() {
    this.room.find(FIND_MY_STRUCTURES)
      .filter((structure) => structure.structureType === STRUCTURE_TOWER)
      .forEach((structure) => {
        const tower = structure as StructureTower;

        const closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
          filter: (target) => target.hits < target.hitsMax
        });
        if (closestDamagedStructure) {
          tower.repair(closestDamagedStructure);
        }

        const closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if (closestHostile) {
          tower.attack(closestHostile);
        }
      });
  }

  private spawnCreepsByRole(creepRole: CreepRole) {
    const creepsByRole = _.filter(Game.creeps, (creep) => CreepUtils.getMemory(creep).role === creepRole.getRole());

    if (this.room.energyAvailable >= creepRole.getRequiredEnergy()
      && creepsByRole.length < creepRole.roleLimit
      && !this.spawning) {
      CreepUtils.spawnLocalCreep(this.room.find(FIND_MY_SPAWNS)[0], creepRole);
      this.spawning = true;
    }
  }

  private orderMiner(creep: Creep) {
    this.fallbackStrategy.orderCreep(creep);
  }

  private orderUpgrader(creep: Creep) {
    this.fallbackStrategy.orderCreep(creep);
  }

  private orderBuilder(creep: Creep) {
    this.fallbackStrategy.orderCreep(creep);
  }

  private orderRepairer(creep: Creep) {
    this.fallbackStrategy.orderCreep(creep);
  }

  private orderHauler(creep: Creep) {
    this.fallbackStrategy.orderCreep(creep);
  }

}
