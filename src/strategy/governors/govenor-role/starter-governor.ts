import {ArchitectManager} from "../../../architect/architect-manager";
import {CreepAction} from "../../../creep/creep-action";
import {CreepRole} from "../../../creep/creep-role";
import {CreepUtils} from "../../../creep/creep-utils";
import {Builder} from "../../../creep/role/builder/builder";
import {LightBuilder} from "../../../creep/role/builder/light-builder";
import {Hauler} from "../../../creep/role/hauler/hauler";
import {LightHauler} from "../../../creep/role/hauler/light-hauler";
import {LightMiner} from "../../../creep/role/miner/light-miner";
import {Miner} from "../../../creep/role/miner/miner";
import {LightRepairer} from "../../../creep/role/repairer/light-repairer";
import {Repairer} from "../../../creep/role/repairer/repairer";
import {LightUpgrader} from "../../../creep/role/upgrader/light-upgrader";
import {Upgrader} from "../../../creep/role/upgrader/upgrader";
import {ResourceMemoryContainer} from "../../../memory/resource-memory-container";
import {GovernorRole} from "../governor-role";

export class StarterGovernor implements GovernorRole {

  private defaultMiner: Miner;
  private defaultUpgrader: Upgrader;
  private defaultRepairer: Repairer;
  private defaultBuilder: Builder;
  private defaultHauler: Hauler;
  private architectManager: ArchitectManager = new ArchitectManager();

  private spawning: boolean = false;
  private room: Room;

  constructor(room: Room, sources: ResourceMemoryContainer[], constructionsSites: number) {
    this.room = room;

    this.defaultMiner = new LightMiner(sources.length);
    this.defaultHauler = new LightHauler(sources.length + 1);
    this.defaultUpgrader = new LightUpgrader(2);
    this.defaultRepairer = new LightRepairer(1);
    this.defaultBuilder = new LightBuilder(constructionsSites > 0 ? 3 : 0);

  }

  public spawnCreeps(): void {

    const miners = _.filter(Game.creeps, (creep) => CreepUtils.getMemory(creep).role === this.defaultMiner.getRole());
    const haulers = _.filter(Game.creeps, (creep) => CreepUtils.getMemory(creep).role === this.defaultHauler.getRole());

    if (miners.length === 0) {
      this.spawnCreepsByRole(this.defaultMiner);
    } else if (haulers.length === 0) {
      this.spawnCreepsByRole(this.defaultHauler);
    } else {
      this.spawnCreepsByRole(this.defaultHauler);
      this.spawnCreepsByRole(this.defaultMiner);
      this.spawnCreepsByRole(this.defaultBuilder);
      this.spawnCreepsByRole(this.defaultUpgrader);
      this.spawnCreepsByRole(this.defaultRepairer);

    }

  }

  public orderCreep(creep: Creep): void {

    switch (CreepUtils.getMemory(creep).role) {
      case "Miner":
        this.orderMiner(creep);
        break;
      case "Hauler":
        this.orderHauler(creep);
        break;
      case "Builder":
        this.orderBuilder(creep);
        break;
      case "Repairer":
        this.orderRepairer(creep);
        break;
      case "Upgrader":
        this.orderUpgrader(creep);
        break;
      default:
        break;

    }
  }

  public construction(): void {
    const rooms = _.filter(Game.rooms, (room) => room.controller != null);
    rooms.forEach((room) => this.architectManager.flagConstructions(room.controller!));
    rooms.forEach((room) => this.architectManager.implementConstructions(room.controller!));
  }

  public orderTowers() {
    this.room.find(FIND_MY_STRUCTURES)
      .filter((structure) => structure.structureType === STRUCTURE_TOWER)
      .forEach((structure) => {
        const tower = structure as StructureTower;

        const closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
          filter: (target) => target.hits < target.hitsMax
        });
        if (closestDamagedStructure) {
          tower.repair(closestDamagedStructure);
        }

        const closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if (closestHostile) {
          tower.attack(closestHostile);
        }
      });
  }

  private spawnCreepsByRole(creepRole: CreepRole) {
    const creepsByRole = _.filter(Game.creeps, (creep) => CreepUtils.getMemory(creep).role === creepRole.getRole());

    if (this.room.energyAvailable >= creepRole.getRequiredEnergy()
      && creepsByRole.length < creepRole.roleLimit
      && !this.spawning) {
      CreepUtils.spawnLocalCreep(this.room.find(FIND_MY_SPAWNS)[0], creepRole);
      this.spawning = true;
    }
  }

  private orderMiner(creep: Creep) {

    switch (CreepUtils.getAction(creep)) {
      case CreepAction.IDLE:
        CreepUtils.assignDedicatedSource(creep);
        break;
      case CreepAction.POSITIONING:
        CreepUtils.moveToAssignedResourceContainer(creep);
        break;
      case CreepAction.MINING:
        CreepUtils.mineAdjacentSource(creep);
        break;
      case CreepAction.RECYCLING:
        CreepUtils.setAction(creep, CreepAction.IDLE);
        break;
      default:
        CreepUtils.setAction(creep, CreepAction.IDLE);
    }
  }

  private orderUpgrader(creep: Creep) {
    if (creep.carry.energy === creep.carryCapacity) {
      CreepUtils.setAction(creep, CreepAction.UPGRADING);
    }
    if (creep.carry.energy === 0) {
      CreepUtils.setAction(creep, CreepAction.IDLE);
    }

    switch (CreepUtils.getAction(creep)) {
      case CreepAction.IDLE:
        this.setAndCollectEnergy(creep);
        break;
      case CreepAction.REFILLING:
        this.setAndCollectEnergy(creep);
        break;
      case CreepAction.UPGRADING:
        CreepUtils.upgrade(creep, this.room.controller!);
        break;
      case CreepAction.RECYCLING:
        CreepUtils.setAction(creep, CreepAction.IDLE);
        break;
    }

  }

  private orderBuilder(creep: Creep) {

    if (creep.carry.energy === creep.carryCapacity && CreepUtils.getAction(creep) !== CreepAction.RECYCLING) {
      CreepUtils.setAction(creep, CreepAction.BUILDING);
    }
    if (creep.carry.energy === 0) {
      CreepUtils.setAction(creep, CreepAction.IDLE);
    }

    switch (CreepUtils.getAction(creep)) {
      case CreepAction.IDLE:
        this.setAndCollectEnergy(creep);
        break;
      case CreepAction.REFILLING:
        this.setAndCollectEnergy(creep);
        break;
      case CreepAction.BUILDING:
        const constructionSiteByPriority = this.getConstructionSiteByPriority();
        if (constructionSiteByPriority === null) {
          CreepUtils.setAction(creep, CreepAction.RECYCLING);
        } else {
          CreepUtils.build(creep, constructionSiteByPriority);
        }
        break;
      case CreepAction.RECYCLING:
        CreepUtils.setAction(creep, CreepAction.IDLE);
        CreepUtils.getMemory(creep).role = "Repairer";
        break;
    }

  }

  private orderRepairer(creep: Creep) {
    if (creep.carry.energy === creep.carryCapacity) {
      CreepUtils.setAction(creep, CreepAction.REPAIRING);
    }
    if (creep.carry.energy === 0) {
      CreepUtils.setAction(creep, CreepAction.IDLE);
    }

    switch (CreepUtils.getAction(creep)) {
      case CreepAction.IDLE:
        this.setAndCollectEnergy(creep);
        break;
      case CreepAction.REFILLING:
        this.setAndCollectEnergy(creep);
        break;
      case CreepAction.REPAIRING:
        const structures = creep.room.find(FIND_STRUCTURES).filter((structure) => structure.hits < structure.hitsMax
          && (structure.structureType === STRUCTURE_ROAD || (structure.structureType === STRUCTURE_WALL && structure.hits < 50_000)))
          .concat(creep.room.find(FIND_MY_STRUCTURES).filter((structure) => structure.hits < structure.hitsMax && structure.hits < 50_000))
          .sort((a, b) => creep.pos.getRangeTo(a.pos) - creep.pos.getRangeTo(b.pos));
        if (structures.length > 0) {
          CreepUtils.repair(creep, structures[0]);
        } else {
          CreepUtils.upgrade(creep, this.room.controller!);
        }
        break;
      case CreepAction.RECYCLING:
        CreepUtils.setAction(creep, CreepAction.IDLE);
        break;
    }

  }

  private orderHauler(creep: Creep) {

    switch (CreepUtils.getAction(creep)) {
      case CreepAction.IDLE:
        CreepUtils.getMemory(creep).actionMemory.targetId = undefined;
        let success = CreepUtils.assignNonStoredEnergyForCollection(creep);
        if (!success) {
          success = CreepUtils.assignPickupContainerEnergy(creep);
        }
        break;
      case CreepAction.CLEANING:
        if (creep.carry.energy === creep.carryCapacity) {
          if (!CreepUtils.attemptStoreInAdjacentContainer(creep)) {
            CreepUtils.setAction(creep, CreepAction.OFFLOADING);
            CreepUtils.getMemory(creep).actionMemory.targetId = undefined;
          }
        } else {
          CreepUtils.moveToTargetAndCollect(creep);
        }
        break;
      case CreepAction.COLLECTING:
        if (creep.carry.energy === creep.carryCapacity) {
          CreepUtils.setAction(creep, CreepAction.OFFLOADING);
          CreepUtils.getMemory(creep).actionMemory.targetId = undefined;
        } else {
          CreepUtils.moveToTargetAndCollect(creep);
        }
        break;
      case CreepAction.OFFLOADING:
        if (creep.carry.energy === 0) {
          CreepUtils.setAction(creep, CreepAction.IDLE);
        } else {
          CreepUtils.offLoadEnergy(creep);
        }
        break;
      case CreepAction.RECYCLING:
        CreepUtils.setAction(creep, CreepAction.IDLE);
        break;
      default:
        CreepUtils.setAction(creep, CreepAction.IDLE);
    }

  }

  private getConstructionSiteByPriority(): ConstructionSite | null {

    const buildSites = this.room.find(FIND_MY_CONSTRUCTION_SITES)
      .sort((a, b) => {
        if (this.getPriority(a) < this.getPriority(b)) {
          return -1;
        }
        if (this.getPriority(a) > this.getPriority(b)) {
          return 1;
        }
        return 0;
      });

    if (buildSites.length > 0) {

      return buildSites[0];

    }
    return null;
  }

  private setAndCollectEnergyUnusedFirst(creep: Creep) {
    if (!CreepUtils.collectEnergyFromClosestDroppedPile(creep)) {
      CreepUtils.collectEnergyFromClosestContainer(creep);
    }
  }

  private setAndCollectEnergy(creep: Creep) {
    if (!CreepUtils.collectEnergyFromClosestContainer(creep)) {
      CreepUtils.collectEnergyFromClosestDroppedPile(creep);
    }
  }

  private getPriority(constructionSite: ConstructionSite): number {
    if (constructionSite.structureType === STRUCTURE_EXTENSION) {
      return 1;
    }
    if (constructionSite.structureType === STRUCTURE_TOWER) {
      return 2;
    }
    if (constructionSite.structureType === STRUCTURE_CONTAINER) {
      return 3;
    }
    if (constructionSite.structureType === STRUCTURE_EXTRACTOR) {
      return 4;
    }
    if (constructionSite.structureType === STRUCTURE_ROAD) {
      return 5;
    }

    return 99;

  }
}
