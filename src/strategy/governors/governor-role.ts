export interface GovernorRole {
  spawnCreeps(): void;

  orderCreep(creep: Creep): void;

  construction(): void;

  orderTowers(): void;
}
