import {GovernorFactory} from "./governors/governor-factory";
import {GovernorRole} from "./governors/governor-role";
import {CreepUtils} from "../creep/creep-utils";

export class MainStrategy {
  public run() {

    const governors: { [roomName: string]: GovernorRole } = {};

    _.forEach(Game.rooms, (room) => {
      governors[room.name] = GovernorFactory.getGovernor(room);
      governors[room.name].construction();
      governors[room.name].spawnCreeps();
      governors[room.name].orderTowers();
    });

    _.forEach(Game.creeps, (creep) => {
      if (CreepUtils.getMemory(creep).localAgent) {
        governors[creep.room.name].orderCreep(creep);
      } else {
        governors[creep.room.name].orderCreep(creep);
      }
    });
  }
}
