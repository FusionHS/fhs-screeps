// type shim for nodejs' `require()` syntax
// for stricter node.js typings, remove this and install `@types/node`
declare const require: (module: string) => any;

// add your custom typings here
declare const __BUILD_TIME__: string;
declare const PRODUCTION: string;
declare const __HOME_NAME__: string;
