import {ResourceMemoryContainer} from "./resource-memory-container";

export class RoomMemoryContainer {
  public defenceWallsBuilt: boolean = false;
  constructor(public ownedResources: ResourceMemoryContainer[]) {
  }
}
