import {ActionMemory} from "./action-memory";

export class CreepMemoryContainer {
  constructor(public role: string,
              public subRole: string,
              public responsiblity: string,
              public localAgent: boolean,
              public actionMemory: ActionMemory) {
  }
}
