export class ResourceMemoryContainer {
  public containerFlagId?: string;

  constructor(public resourceId: string, public availableSpots: number, public resourceType: LOOK_SOURCES | LOOK_MINERALS) {
  }

}
