export class MemoryInit {
  public static init() {

    if (Memory.startTick == null) {
      Memory.startTick = Game.time;
    }

    if (Memory.homeRoom == null) {
      Memory.homeRoom = Game.rooms[Object.keys(Game.rooms)[0]].name;

      Object.keys(Game.rooms).forEach((key) => {
        if (key === __HOME_NAME__) {
          Memory.homeRoom = Game.rooms[key].name;
        }
      });
    }

    if (Memory.rooms == null) {
      Memory.rooms = {};
    }
  }

}
