import {CreepAction} from "../creep/creep-action";

export interface ActionMemory {
  action: CreepAction;
  targetId?: string;
}
