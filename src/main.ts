import {ErrorMapper} from "utils/ErrorMapper";
import {Cleanup} from "./cleanup";
import {MemoryInit} from "./memory/memory-init";
import {MainStrategy} from "./strategy/main-strategy";

export const loop = ErrorMapper.wrapLoop(() => {

  MemoryInit.init();

  Cleanup.cleanupCreeps();

  const mainStrategy = new MainStrategy();
  mainStrategy.run();

});
