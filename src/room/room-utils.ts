import {FlagType} from "../flag/flag-type";
import {ResourceMemoryContainer} from "../memory/resource-memory-container";
import {RoomMemoryContainer} from "../memory/room-memory-container";

export class RoomUtils {

  private static costMatrix: CostMatrix;

  public static getRoomMemory(room: Room): RoomMemoryContainer {
    if (Memory.rooms[room.name] == null) {
      return this.discoveryResources(room);
    } else {
      return Memory.rooms[room.name] as RoomMemoryContainer;
    }
  }

  public static getNumberOfWalkableSpacesNextToPos(roomPosition: RoomPosition): number {
    return this.getWalkableSpacesNextToPos(roomPosition, 1).length;
  }

  public static getWalkableSpacesNextToPos(roomPosition: RoomPosition, range: number) {

    return Game.rooms[roomPosition.roomName].lookForAtArea(LOOK_TERRAIN,
      roomPosition.y - range,
      roomPosition.x - range,
      roomPosition.y + range,
      roomPosition.x + range,
      true)
      .filter((area) => area.terrain !== "wall");
  }

  public static getAnyBuildableSpacesNextToPos(roomPosition: RoomPosition): RoomPosition[] {
    return this.getWalkableSpacesNextToPos(roomPosition, 1)
      .map((area) => new RoomPosition(area.x, area.y, roomPosition.roomName))
      .filter((pos) => this.isBuildable(pos));
  }

  public static getClosestBuildableSpaceNextToTarget(target: RoomObject, from: RoomObject): RoomPosition {
    let roomPosition = this.getShortestPathToObject(from, target).path[0];

    if (!this.isBuildable(roomPosition)) {
      roomPosition = this.minimizeMovePathToTargetPos(from.pos, this.getAnyBuildableSpacesNextToPos(roomPosition)).path[0];
    }

    return roomPosition;
  }

  public static getClosestBuildableSpace(target: RoomObject): RoomPosition {
    let range = 1;
    let foundBuildablePos = false;
    let roomPosition: RoomPosition[] = [];
    while (!foundBuildablePos) {
      roomPosition = this.getWalkableSpacesNextToPos(target.pos, range)
        .map((area) => new RoomPosition(area.x, area.y, target.pos.roomName))
        .filter((pos) => this.isBuildable(pos));

      if (roomPosition.length > 0) {
        foundBuildablePos = true;
      } else {
        range++;
      }

    }

    return roomPosition[0];
  }

  public static getClosestBuildableArea(target: RoomObject): RoomPosition[] {
    let range = 1;
    let foundBuildablePos = false;
    let roomPosition: RoomPosition[] = [];
    while (!foundBuildablePos) {
      roomPosition = this.getWalkableSpacesNextToPos(target.pos, range)
        .map((area) => new RoomPosition(area.x, area.y, target.pos.roomName))
        .filter((pos) => this.isBuildable(pos));

      if (roomPosition.length > 0) {
        foundBuildablePos = true;
      } else {
        range++;
      }

    }

    return roomPosition;
  }

  public static getShortestPathToObject(start: RoomObject, finish: RoomObject) {
    return PathFinder.search(start.pos, {pos: finish.pos, range: 1},
      {
        plainCost: 2,
        swampCost: 2,
        roomCallback: (roomName) => this.getCostMatrix(roomName)

      }
    );
  }

  public static getShortestPathToWalkablePos(start: RoomObject, finish: RoomObject) {
    return PathFinder.search(start.pos, finish.pos,
      {
        plainCost: 2,
        swampCost: 2,
        roomCallback: (roomName) => this.getCostMatrix(roomName)
      }
    );
  }

  public static createFlag(pos: RoomPosition, flagType: FlagType): string | undefined {
    const room = Game.rooms[pos.roomName];
    const result = room.createFlag(pos, flagType.toString() + "-" + pos.x + "-" + pos.y + "-" + pos.roomName);
    if (result !== ERR_NAME_EXISTS && result !== ERR_INVALID_ARGS) {
      return result;
    }
    return undefined;

  }

  public static getMovementCost(from: RoomPosition, to: RoomPosition): number {
    return PathFinder.search(from, {pos: to, range: 1}).cost;
  }

  private static discoveryResources(room: Room): RoomMemoryContainer {
    let resources = room.find(FIND_SOURCES).map((resource) => {
      return new ResourceMemoryContainer(resource.id, this.getSpots(resource), LOOK_SOURCES);
    });
    resources = resources.concat(room.find(FIND_MINERALS).map((resource) => {
      return new ResourceMemoryContainer(resource.id, this.getSpots(resource), LOOK_MINERALS);
    }));
    const roomMemoryContainer = new RoomMemoryContainer(resources);
    Memory.rooms[room.name] = roomMemoryContainer;
    return roomMemoryContainer;
  }

  private static getSpots(resource: Source | Mineral): number {
    return this.getNumberOfWalkableSpacesNextToPos(resource.pos);
  }

  private static isBuildable(pos: RoomPosition): boolean {
    return pos.look()
      .filter((item) => item.type === LOOK_STRUCTURES || item.type === LOOK_CONSTRUCTION_SITES).length === 0;
  }

  private static minimizeMovePathToTargetPos(from: RoomPosition, possibleLocations: RoomPosition[]) {
    return possibleLocations.map((pos) => PathFinder.search(from, possibleLocations)).sort((a, b) => a.cost - b.cost)[0];
  }

  private static getCostMatrix(roomName: string): CostMatrix {

    if (this.costMatrix !== undefined) {
      return this.costMatrix;
    }

    const room = Game.rooms[roomName];

    const costs = new PathFinder.CostMatrix;

    room.find(FIND_STRUCTURES).forEach((struct) => {
      if (struct.structureType === STRUCTURE_ROAD) {
        // Favor roads over plain tiles
        costs.set(struct.pos.x, struct.pos.y, 1);
      } else if (struct.structureType !== STRUCTURE_CONTAINER &&
        (struct.structureType !== STRUCTURE_RAMPART ||
          !struct.my)) {
        // Can't walk through non-walkable buildings
        costs.set(struct.pos.x, struct.pos.y, 0xff);
      }
    });

    room.find(FIND_CONSTRUCTION_SITES).forEach((struct) => {
      if (struct.structureType === STRUCTURE_WALL) {
        // Can't walk through non-walkable buildings
        costs.set(struct.pos.x, struct.pos.y, 0xff);
      }
    });
    
    this.costMatrix = costs;
    return costs;
  }
}
