"use strict";

import replace from 'rollup-plugin-replace';
import clear from "rollup-plugin-clear";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import typescript from "rollup-plugin-typescript2";
import screeps from "rollup-plugin-screeps";

const isProduction = process.env.NODE_ENV === 'production';
const isPServer = process.env.NODE_ENV === 'pserver';

// use the `main` target from `screeps.json` in production mode
var cfg = isProduction ? 'main' : 'sim';
cfg = isPServer ? 'pserver' : cfg;

// let cfg;
// const dest = process.env.DEST;
// if (!dest) {
//   console.log("No destination specified - code will be compiled but not uploaded");
// } else if ((cfg = require("./screeps")[dest]) == null) {
//   throw new Error("Invalid upload destination");
// }

export default {
  input: "src/main.ts",
  output: {
    file: "dist/main.js",
    format: "cjs",
    sourcemap: true
  },

  plugins: [
    clear({targets: ["dist"]}),
    resolve(),
    commonjs({
      namedExports: {
        'node_modules/screeps-profiler/screeps-profiler.js': ['screeps-profiler']
      }
    }),
    typescript({tsconfig: "./tsconfig.json"}),
    screeps({
      config: require("./screeps")[cfg],
      // if `NODE_ENV` is local, perform a dry run
      dryRun: process.env.NODE_ENV === 'local'
    }),
    replace({
      // returns 'true' if code is bundled in prod mode
      PRODUCTION: JSON.stringify(isProduction),
      __HOME_NAME__: JSON.stringify(isProduction ? "TheCrusade" : "Spawn1"),
      // you can also use this to include deploy-related data, such as
      // date + time of build, as well as latest commit ID from git
      __BUILD_TIME__: JSON.stringify(Date.now())
    })
  ]
}
